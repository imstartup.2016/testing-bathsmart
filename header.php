<!DOCTYPE html>
<html>
<head>
    <title>Front-end Test-- Login</title>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="fonts/font.css">
    <link rel="stylesheet" type="java" href="">
    <script type="text/javascript" src="lib/jquery.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body>