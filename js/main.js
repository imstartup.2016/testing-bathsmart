$(document).ready(function() { 
    console.log('start');

    // Validate E-mail form
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function validate() {
        $("#result").text("");
        var email = $("#inputEmail").val();

        if (validateEmail(email)) {
            $("#result").text(email + " is valid :)");
            $("#result").css("color", "green");
            $("#inputEmail").attr('data-validate','pass');

        } else {
            $("#result").text(email + " is not valid :(");
            $("#result").css("color", "#E82C0C");
            $("#inputEmail").attr('data-validate','fail');
        }

        if (document.getElementById('inputPassword').value == 'hello') {
            console.log('Correct');
            $("#result-password").empty();
            $("#inputPassword").attr('data-validate','pass');

        } else {
            console.log('wrong');
            $("#result-password").text( "The password your entered is incorrect.");
            $("#result-password").css("color", "#E82C0C");
            $("#inputPassword").attr('data-validate','fail');

        }

        return false;
    }

    // Validate a password input
    $("#validate").bind("click", validate );
    $("button[type='submit']").on("click", function() {
        var checkdata01 = $("#inputEmail").attr('data-validate');
        var checkdata02 = $("#inputPassword").attr('data-validate');
        console.log("mail =" + checkdata01 , "pw =" + checkdata02);
        if (checkdata02  == 'pass' && checkdata01 == 'pass') {
            console.log('01 pass');
            window.location.href = "secound.php";
        }
    });



    // Function Gee data from http://date.jsontest.com/
    // Display the date and time by realtime

    (function updateTime() {
        $.ajax({
            url: 'http://date.jsontest.com/', 
            success: function(data) {
                $('#time').text(data.time);
                $('#date').text(data.date);
            },
            complete: function() {
              setTimeout(updateTime, 1000);
            }
        });
    })();

    $('#nav-icon').click(function(){
        $(this).toggleClass('open');
    });

    $('#content-box').load('include/content01.php');


    $('.list-menu a').on('click', function() {
        var localHref = $(this).parent().data('href');
        $('#content-box').load(localHref);

        $('.list-menu a').removeClass('active');
        $(this).addClass('active');
    });
});