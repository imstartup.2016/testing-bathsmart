<?php include('header.php'); ?>
<div class="container container-secound">
    <div class="row">
        <div class="col-md-3">
            <h2 class="title-logo">Front-end Test</h2>
        </div>
        <div class="col-md-9 col-date-time">
            <span id="time">now</span>   
            <span id="date">now</span>   
        </div>
    </div>
    <div class="row line-top">
        <div class="col-md-3">
            <ul class="menu-left-bar">
                <li class="list-menu" data-href="include/content01.php"><a class="active">Menu 01</a></li>
                <li class="list-menu" data-href="include/content02.php"><a>Menu 02</a></li>
                <li class="list-menu" data-href="include/content03.php"><a>Menu 03</a></li>
                <li class="list-menu" data-href="include/content04.php"><a>Menu 04</a></li>
                <li class="list-menu" data-href="include/content05.php"><a>Menu 05</a></li>
                <li class="list-menu" data-href="include/content06.php"><a>Menu 06</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <div id="content-box" class="infomation-text"></div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>